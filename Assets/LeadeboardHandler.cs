using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeadeboardHandler : MonoBehaviour
{
    UserScoreHandler[] userScores;
    ScoreController scoreController;
    List<UserData> usersData;
    private void Awake()
    {
        scoreController = FindObjectOfType<ScoreController>();
        userScores = GetComponentsInChildren<UserScoreHandler>();
    }
    public void SetLeaderBoard()
    {
        usersData = scoreController.Users;
        for (int i=0;i<usersData.Count;i++)
        {
            userScores[i].SetUser(usersData[i].username, usersData[i].score);
        }
    }
}
