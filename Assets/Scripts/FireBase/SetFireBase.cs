using Firebase;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetFireBase : MonoBehaviour
{
    private void Awake()
    {
        // Configura la variable de entorno para usar el emulador de autenticación
        System.Environment.SetEnvironmentVariable("USE_AUTH_EMULATOR", "true");

        // Inicializa Firebase
        Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
            FirebaseApp app = FirebaseApp.DefaultInstance;
        });

    }
}
