using Firebase.Auth;
using Firebase.Database;
using Firebase.Extensions;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Rendering.Universal;

public class UsernameHandler : MonoBehaviour
{
    [SerializeField] TMP_Text username;
    private CanvasGroup canvasGroup;
    private void Reset()
    {
        username = GetComponent<TMP_Text>();
    }
    private void Awake()
    {
        username = GetComponent<TMP_Text>();
        canvasGroup = GetComponent<CanvasGroup>();
    }
    private void Start()
    {
        FirebaseAuth.DefaultInstance.StateChanged += HandleAuthChange;
    }

    private void HandleAuthChange(object sender, EventArgs e)
    {
        var currentUser = FirebaseAuth.DefaultInstance.CurrentUser;
        if (currentUser != null)
        {
            canvasGroup.alpha = 1;
            SetCurrentUsername(currentUser.UserId);
        }
        else
        {
            canvasGroup.alpha = 0;
        }
    }
    void SetCurrentUsername(string userId) {
        FirebaseDatabase.DefaultInstance
        .GetReference("users/"+userId+"/username")
        .GetValueAsync().ContinueWithOnMainThread(task => {
            if (task.IsFaulted)
            {
                Debug.Log(task.Exception);
            }
            else if (task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result;
                username.text = (string)snapshot.Value;
                Debug.Log("username"+(string)snapshot.Value);
            }
        });
    }
}
