using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FlappyBird
{
    public class PipeSpawner : MonoBehaviour
    {
        [SerializeField]
        private GameObject pipe;
        [SerializeField]
        private Transform spawnPoint;

        [Space, SerializeField, Range(-1, 1)]
        private float minHeight;
        [SerializeField, Range(-1, 1)]
        private float maxHeight;

        [Space, SerializeField]
        private float timeToSpawnFirstPipe;
        [SerializeField]
        private float timeToSpawnPipe;

        //Object Pooling
        public static PipeSpawner SharedInstance;
        public List<GameObject> pooledObjects;
        public GameObject objectToPool;
        public int amountToPool;

        public delegate void PipeSpawnDelegate();

        private void Awake()
        {
            if (SharedInstance == null)
            {
                SharedInstance = this;
                DontDestroyOnLoad(gameObject);
            }
        }
        public static PipeSpawner PipeSpawnerInstance()
        {
            return SharedInstance;
        }
        private void Start()
        {
            pooledObjects = new List<GameObject>();
            GameObject tmp;
            for (int i = 0; i < amountToPool; i++)
            {
                tmp = Instantiate(objectToPool);
                tmp.SetActive(false);
                pooledObjects.Add(tmp);
            }
            StartCoroutine(SpawnPipes());
        }

        private Vector3 GetSpawnPosition()
        {
            return new Vector3(spawnPoint.position.x, Random.Range(minHeight, maxHeight), spawnPoint.position.z);
        }

        private IEnumerator SpawnPipes()
        {
            yield return new WaitForSeconds(timeToSpawnFirstPipe);
            GameObject temp = PipeSpawner.PipeSpawnerInstance().GetPooledObject();
            if (temp != null)
            {
                temp.transform.position = GetSpawnPosition();
                temp.SetActive(true);
                temp.GetComponent<Pipe>().StartDestroyPipe();
            }
            do
            {
                yield return new WaitForSeconds(timeToSpawnPipe);
                temp = PipeSpawner.PipeSpawnerInstance().GetPooledObject();
                if (temp != null)
                {
                    temp.transform.position = GetSpawnPosition();
                    temp.SetActive(true);
                    temp.GetComponent<Pipe>().StartDestroyPipe();
                }
            } while (true);
        }

        public void Stop()
        {
            StopAllCoroutines();
        }
        public GameObject GetPooledObject()
        {
            for (int i = 0; i < amountToPool; i++)
            {
                if (!pooledObjects[i].activeInHierarchy)
                {
                    return pooledObjects[i];
                }
            }
            return null;
        }
    }
}