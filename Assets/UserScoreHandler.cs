using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UserScoreHandler : MonoBehaviour
{
    [SerializeField] TMP_Text username;
    [SerializeField] TMP_Text score;

    public void SetUser(string username,int score)
    {
        this.username.text= username;
        this.score.text = score.ToString();
    }
}
