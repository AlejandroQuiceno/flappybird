using Firebase.Auth;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LoadSceneWhenUserAuths : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    private GameObject authPanel;
    public UnityEvent OnAuth;
    void Start()
    {
        FirebaseAuth.DefaultInstance.StateChanged += HandledAuthStateChanged;
    }

    private void HandledAuthStateChanged(object sender, EventArgs e)
    {
        if(FirebaseAuth.DefaultInstance.CurrentUser != null && authPanel.activeSelf)
        {
            Debug.Log("user logged in with token");
            authPanel.SetActive(false);
            Debug.Log("User Logged in with email: "+FirebaseAuth.DefaultInstance.CurrentUser.Email);
            OnAuth?.Invoke();
        }
    }

    private void OnDestroy()
    {
        FirebaseAuth.DefaultInstance.StateChanged += HandledAuthStateChanged;
    }
}
