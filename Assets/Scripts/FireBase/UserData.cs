﻿public class UserData
{
    public int score;
    public string username;
    public UserData(string username,int score)
    {
        this.score = score;
        this.username = username;
    }
}