using Firebase.Auth;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
public class ButtonLogin: MonoBehaviour
{
    [SerializeField] private Button _loginButton;
    private Coroutine Register;
    public UnityEvent OnLogin;

    private void Reset()
    {
        _loginButton = GetComponent<Button>();
    }
    private void Start()
    {
        _loginButton.onClick.AddListener(HandleLoginButtonClicked);
    }

    private void HandleLoginButtonClicked()
    {
        string email = GameObject.Find("e-mail").GetComponent<TMP_InputField>().text;
        string password = GameObject.Find("password").GetComponent<TMP_InputField>().text;
        var auth = FirebaseAuth.DefaultInstance;
        auth.SignInWithEmailAndPasswordAsync(email, password).ContinueWith(task => {
            if (task.IsCanceled)
            {
                Debug.LogError("SignInWithEmailAndPasswordAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                Debug.LogError("SignInWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                return;
            }
            OnLogin?.Invoke();
            Firebase.Auth.AuthResult result = task.Result;
            Debug.LogFormat("User signed in successfully: {0} ({1})",
                result.User.DisplayName, result.User.UserId);
        });
    }
}
