using Firebase.Auth;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ButtonLogout : MonoBehaviour
{
    [SerializeField] private Button _logoutButton;
    [SerializeField] private GameObject authPanel;
    private void Reset()
    {
        _logoutButton = GetComponent<Button>();
    }
    private void Start()
    {
        _logoutButton.onClick.AddListener(HandleLogoutButtonClicked);
    }

    private void HandleLogoutButtonClicked()
    {
        FirebaseAuth.DefaultInstance.SignOut();
        authPanel.SetActive(true);
    }
}
