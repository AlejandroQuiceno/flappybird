using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Firebase.Auth;
using Firebase.Extensions;
using Firebase;
public class ResetPassword : MonoBehaviour
{
    [SerializeField] TMP_InputField email;
    public void forgetPasswordSubmit ()
    {
        if(email.text == string.Empty)
        {
            return;
        }
        string emailText = email.text;
        FirebaseAuth.DefaultInstance.SendPasswordResetEmailAsync(emailText).ContinueWithOnMainThread(task =>
        {
            if (task.IsCanceled)
            {
                Debug.LogError("SendPasswordResetEmailAsync was canceled");
                return;
            }
            if (task.IsFaulted)
            {
                Debug.LogError("SendPasswordResetEmailAsync encountered an error");
                return;
            }

        });
    }
}
