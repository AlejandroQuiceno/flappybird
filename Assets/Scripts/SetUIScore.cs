using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Firebase.Auth;
using System;

public class SetUIScore : MonoBehaviour
{
    TMP_Text score;
    ScoreController scoreController;
    private void Awake()
    {
        score = GetComponent<TMP_Text>();
        scoreController = FindObjectOfType<ScoreController>();
    }
    private void Start()
    {
        FirebaseAuth.DefaultInstance.StateChanged += HandleAuthChange;
    }

    private void HandleAuthChange(object sender, EventArgs e)
    {
        var currentUser = FirebaseAuth.DefaultInstance.CurrentUser;
        if (currentUser == null)
        {
            score.text = string.Empty;
        }
    }

    public void SetTextScore()
    {
        Debug.Log("score gotten");
        this.score.text = scoreController.Score.ToString();
    }
}
