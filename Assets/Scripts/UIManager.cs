using UnityEngine;
using TMPro;

namespace FlappyBird
{
    public class UIManager : MonoBehaviour
    {
        [SerializeField]
        private TextMeshProUGUI score;
        private ScoreController scoreController;
        private void Awake()
        {
            scoreController = FindObjectOfType<ScoreController>();
        }
        public void UpdateScore()
        {
            if (score != null)
            {
                score.text = GameManager.Instance.scoreCount.ToString();
                WriteScore();
            }
        }
        public void WriteScore()
        {
            if(scoreController.Score< GameManager.Instance.scoreCount)
            {
                scoreController.WriteNewScore(GameManager.Instance.scoreCount);
            }
        }
    }
}