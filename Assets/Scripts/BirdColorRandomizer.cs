using FlappyBird;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdColorRandomizer : MonoBehaviour
{
    [SerializeField] GameObject[] birds;
    Bird bird;
    private void Awake()
    {
        bird= GetComponent<Bird>();
    }
    void Start()
    {
        int index = UnityEngine.Random.RandomRange(0, 3);
        for(int i =0;i<birds.Length;i++)
        {
            if(i!=index)
            {
                birds[i].SetActive(false);
            }
        }
        bird.animator = birds[index].GetComponent<Animator>();
    }
}
