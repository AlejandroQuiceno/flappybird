using Firebase.Auth;
using Firebase.Database;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using TMPro;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;
using UnityEngine.UI;
using UnityEngine.Events;
public class buttonSignUp : MonoBehaviour
{
    [SerializeField] private Button _registrationButton;
    [SerializeField] private GameObject authPanel;
    private Coroutine Register;
    DatabaseReference mDataBaseRef;
    public UnityEvent OnSignUp;

    private void Start()
    {
        _registrationButton.onClick.AddListener(HandleRegisterButtonClicked);
        authPanel.SetActive(true);
        mDataBaseRef = FirebaseDatabase.DefaultInstance.RootReference;
    }

    private void HandleRegisterButtonClicked()
    {
        string email = GameObject.Find("e-mail").GetComponent<TMP_InputField>().text;
        string password = GameObject.Find("password").GetComponent<TMP_InputField>().text;
        string username = GameObject.Find("username").GetComponent<TMP_InputField>().text;
        if(email == string.Empty || password == string.Empty || username == string.Empty)
        {
            Debug.Log("Fill all the fields to continue");
            return;
        }
        Register = StartCoroutine(RegisterUser(email, password,username));
    }

    private IEnumerator RegisterUser(string email, string password,string username)
    {
        var auth = FirebaseAuth.DefaultInstance;
        var registerTask = auth.CreateUserWithEmailAndPasswordAsync(email, password);
        yield return new WaitUntil(() => registerTask.IsCompleted);
        if (registerTask.IsCanceled)
        {
            Debug.LogError("SignInWithEmailAndPasswordAsync was canceled.");
        }
        else if (registerTask.IsFaulted)
        {
            Debug.LogError("SignInWithEmailAndPasswordAsync encountered an error: " + registerTask.Exception);
        }
        else
        {
            Firebase.Auth.AuthResult result = registerTask.Result;
            Debug.LogFormat("User signed in successfully: {0} ({1})",
            result.User.DisplayName, result.User.UserId);
            //Register the user in the database root json
            mDataBaseRef.Child("users").Child(result.User.UserId).Child("username").SetValueAsync(username);
            SetInitialScore(result.User.UserId);
            OnSignUp?.Invoke();
        }
        
    }
    private void SetInitialScore(string userID)
    {
        mDataBaseRef.Child("users").Child(userID).Child("score").SetValueAsync(0);
    }
}
