using Firebase.Auth;
using Firebase.Database;
using Firebase.Extensions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class ScoreController : MonoBehaviour
{
    DatabaseReference mDatabase;
    string Userid;
    List<UserData> users;
    int score;
    public int Score { get => score; set => score = value; }
    public List<UserData> Users { get => users; set => users = value; }

    public UnityEvent OnScoreGotten;
    public UnityEvent OnUsersGotten;
    void Start()
    {
        mDatabase = FirebaseDatabase.DefaultInstance.RootReference;
        Userid = FirebaseAuth.DefaultInstance.CurrentUser?.UserId;
        
    }

    public void HandleAuthChange()
    {
        var currentUser = FirebaseAuth.DefaultInstance.CurrentUser;
        if (currentUser != null)
        {
            GetUserScore();
        }
    }

    public void WriteNewScore(int newScore)
    {
        Score = newScore;
        mDatabase.Child("users").Child(Userid).Child("score").SetValueAsync(newScore);
    }
    public void GetUserScore()//debe de llamarse al crear el usuario
    {
        Userid = FirebaseAuth.DefaultInstance.CurrentUser?.UserId;
        FirebaseDatabase.DefaultInstance
        .GetReference("users/" + Userid + "/score")
        .ValueChanged += (sender, args) =>
        {
            if (args.DatabaseError != null)
            {
                Debug.LogError(args.DatabaseError.Message);
                return;
            }

            if (args.Snapshot != null && args.Snapshot.Exists)
            {
                // Data exists; handle it here
                Score = Convert.ToInt32(args.Snapshot.Value);
                OnScoreGotten?.Invoke();
            }
            else
            {
                // Data doesn't exist at the specified path
                Debug.Log("Data does not exist.");
            }
        };
    }
    public void GetUsersHighestScores()
    {
        Debug.Log("Fetching highest scores...");

        DatabaseReference reference = FirebaseDatabase.DefaultInstance.GetReference("users");
        reference.OrderByChild("score").LimitToLast(5).ValueChanged += (sender, args) =>
        {
            if (args.DatabaseError != null)
            {
                Debug.LogError(args.DatabaseError.Message);
                return;
            }

            if (args.Snapshot != null && args.Snapshot.Exists)
            {
                Dictionary<string, object> value = (Dictionary<string, object>)args.Snapshot.Value;
                List<UserData> users = new List<UserData>();

                foreach (var item in value)
                {
                    Dictionary<string, object> userObject = (Dictionary<string, object>)item.Value;
                    string username = userObject["username"].ToString();
                    int score = Convert.ToInt32(userObject["score"]);
                    users.Add(new UserData(username, score));
                }

                // Sort the list by score in descending order
                users = users.OrderByDescending(user => user.score).ToList();

                Users = users; // Update the Users list

                Debug.Log("Highest scores fetched successfully.");
                OnUsersGotten?.Invoke();
            }
            else
            {
                Debug.Log("No data found at the specified path.");
            }
        };
    }

}
